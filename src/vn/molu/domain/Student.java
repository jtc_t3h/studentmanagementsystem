package vn.molu.domain;

import java.io.Serializable;

public class Student implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// instance variable
	private long rowId;
	private String firstName;
	private String lastName;
	private long cunyId;
	private double gpa;
	private String venusLogin;
	
	// method
	
	// Ham khoi tao mac dinh tuong minh
	public Student() {
	}
	
	// Hàm khởi tạo có tham số
	public Student(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Student(long rowId) {
		this.rowId = rowId;
	}

	public Student(long rowId, String firstName, String lastName, long cunyId, double gpa, String venusLogin) {
		this.rowId = rowId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.cunyId = cunyId;
		this.gpa = gpa;
		this.venusLogin = venusLogin;
	}
	
	// Hàm getter và setter
	public long getRowId() {
		return  rowId;
	}
	
	public void setRowId(long rowId) {
		this.rowId = rowId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getCunyId() {
		return cunyId;
	}

	public void setCunyId(long cunyId) {
		this.cunyId = cunyId;
	}

	public double getGpa() {
		return gpa;
	}

	public void setGpa(double gpa) {
		this.gpa = gpa;
	}

	public String getVenusLogin() {
		return venusLogin;
	}

	public void setVenusLogin(String venusLogin) {
		this.venusLogin = venusLogin;
	}
	
	// bussiness method
	public void displayInfor() {
		// In ra thông tin của Student
		System.out.println("Row Id = " + rowId);
		System.out.println("Fist Name = " + firstName);
	}

	@Override
	public String toString() {
		return rowId + "\t" + firstName + " " + lastName + "\t" + cunyId;
	}
	
	
}
