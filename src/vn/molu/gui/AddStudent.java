package vn.molu.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import vn.molu.dao.StudentDAO;
import vn.molu.domain.Student;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AddStudent extends JFrame {
	
	private boolean action;
	
	private JPanel contentPane;
	private JTextField txtFirstName;
	private JTextField txtLastName;
	private JTextField txtCunyID;
	private JTextField txtGPA;
	private JTextField txtVenusLogin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddStudent frame = new AddStudent();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddStudent() {
		setTitle("Create New Student Record");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 314, 240);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblFirstName = new JLabel("First Name:");
		lblFirstName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFirstName.setBounds(6, 11, 95, 16);
		contentPane.add(lblFirstName);
		
		txtFirstName = new JTextField();
		txtFirstName.setBounds(110, 6, 191, 26);
		contentPane.add(txtFirstName);
		txtFirstName.setColumns(10);
		
		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLastName.setBounds(6, 41, 95, 16);
		contentPane.add(lblLastName);
		
		txtLastName = new JTextField();
		txtLastName.setColumns(10);
		txtLastName.setBounds(110, 36, 191, 26);
		contentPane.add(txtLastName);
		
		JLabel lblCunyId = new JLabel("CUNY ID:");
		lblCunyId.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCunyId.setBounds(6, 71, 95, 16);
		contentPane.add(lblCunyId);
		
		txtCunyID = new JTextField();
		txtCunyID.setColumns(10);
		txtCunyID.setBounds(110, 66, 191, 26);
		contentPane.add(txtCunyID);
		
		JLabel lblGpa = new JLabel("GPA:");
		lblGpa.setHorizontalAlignment(SwingConstants.RIGHT);
		lblGpa.setBounds(6, 104, 95, 16);
		contentPane.add(lblGpa);
		
		txtGPA = new JTextField();
		txtGPA.setColumns(10);
		txtGPA.setBounds(110, 99, 191, 26);
		contentPane.add(txtGPA);
		
		JLabel lblVenusLogin = new JLabel("Venus Login:");
		lblVenusLogin.setHorizontalAlignment(SwingConstants.RIGHT);
		lblVenusLogin.setBounds(6, 138, 95, 16);
		contentPane.add(lblVenusLogin);
		
		txtVenusLogin = new JTextField();
		txtVenusLogin.setColumns(10);
		txtVenusLogin.setBounds(110, 133, 191, 26);
		contentPane.add(txtVenusLogin);
		
		JButton btnAddNewStudent = new JButton("Add New Student");
		btnAddNewStudent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// B1. Lấy thông tin từ form -> tạo đối tượng Student
				Student student = new Student();
				student.setFirstName(txtFirstName.getText().trim());
				student.setLastName(txtLastName.getText().trim());
				student.setCunyId(Long.parseLong(txtCunyID.getText().trim()));
				student.setGpa(Double.parseDouble(txtGPA.getText().trim()));
				student.setVenusLogin(txtVenusLogin.getText().trim());
				
				// B2. Insert DB
				StudentDAO dao = new StudentDAO();
				try {
					dao.insert(student);
					action = true;
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
				// B3. Đóng màn form
				dispose();
			}
		});
		btnAddNewStudent.setForeground(Color.BLACK);
		btnAddNewStudent.setBackground(Color.BLUE);
		btnAddNewStudent.setBounds(16, 182, 140, 29);
		contentPane.add(btnAddNewStudent);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancel.setBounds(168, 182, 117, 29);
		contentPane.add(btnCancel);
	}
	
	public boolean isAction() {
		return action;
	}
}
