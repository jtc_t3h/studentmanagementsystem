package vn.molu.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;

import vn.molu.dao.StudentDAO;
import vn.molu.domain.Student;
import vn.molu.utils.JsonUtils;
import vn.molu.utils.XMLUtils;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.awt.event.InputEvent;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ListSelectionModel;
import java.awt.event.KeyAdapter;

public class MainBoard extends JFrame {

	private JPanel contentPane;
	private JTextField txtSearch;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainBoard frame = new MainBoard();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainBoard() {
		setTitle("Student Managment System");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 662, 651);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		JMenuItem mntmOpen = new JMenuItem("Open");
		mntmOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// B1: Open dialog Directory và chọn file -> dùng text file
				File selectedFile = null;

				JFileChooser jfc = new JFileChooser();
				jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				jfc.setCurrentDirectory(new File("/Users/phaolo"));
				if (jfc.showOpenDialog(contentPane) == JFileChooser.APPROVE_OPTION) {
					selectedFile = jfc.getSelectedFile();
				}

				// B2: Đọc nội dung của file và nạp dữ liệu vào trong table
				List<Student> listOfStudent = new ArrayList<Student>();
				if (selectedFile.getName().endsWith("bin")) { // Đọc file bin
					try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(selectedFile))){
						Object temp = null;
						while ( (temp = in.readObject()) != null) {
							Student s = (Student) temp;
							listOfStudent.add(s);
						}
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} else if (selectedFile.getName().endsWith("json")) { // Đọc file theo định dạng json
					try {
						listOfStudent = JsonUtils.simpleReadJson(selectedFile.getAbsolutePath());
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} else if (selectedFile.getName().endsWith("xml")) {
					try {
						listOfStudent = XMLUtils.readXML(selectedFile.getAbsolutePath());
					} catch (ParserConfigurationException e1) {
						e1.printStackTrace();
					} catch (SAXException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				} else { // Đọc file txt
					String content = "";
					try (FileInputStream in = new FileInputStream(selectedFile)) {
						int i;
						while ((i = in.read()) != -1) {
							content += (char) i;
						}
					} catch (Exception ex) {
						ex.printStackTrace();
					}

					// B3: Xử lý dữ liệu -> chuyển content thanh danh sách đối tượng Student
					StringTokenizer st = new StringTokenizer(content, "\n");
					st.nextToken(); // loại bỏ dòng header

//				List<Student> listOfStudent = new ArrayList<Student>();
					while (st.hasMoreTokens()) {
						String token = st.nextToken();

						String temp[] = token.split(",");
						Student student = new Student();
						student.setRowId(Long.parseLong(temp[0].trim()));
						student.setFirstName(temp[1]);
						student.setLastName(temp[2]);
						student.setCunyId(Long.parseLong(temp[3].trim()));
						student.setGpa(Double.parseDouble(temp[4].trim()));
						student.setVenusLogin(temp[5]);

						listOfStudent.add(student);
					}
				}

				// B4: Nạp dữ liệu lên cho Table
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				for (Student s : listOfStudent) {
					model.addRow(new Object[] { s.getRowId(), s.getFirstName(), s.getLastName(), s.getCunyId(),
							s.getGpa(), s.getVenusLogin() });
				}
			}
		});
		mntmOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.META_MASK));
		mnFile.add(mntmOpen);

		JMenuItem mntmExport = new JMenuItem("Export");
		mntmExport.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.META_MASK));
		mnFile.add(mntmExport);

		JSeparator separator = new JSeparator();
		mnFile.add(separator);

		JMenuItem mntmQuit = new JMenuItem("Quit");
		mntmQuit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.META_MASK));
		mnFile.add(mntmQuit);

		JMenu mnAbout = new JMenu("Help");
		menuBar.add(mnAbout);

		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.META_MASK));
		mnAbout.add(mntmAbout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblSearchBy = new JLabel("Search by:");
		lblSearchBy.setBounds(6, 6, 75, 16);
		contentPane.add(lblSearchBy);

		JComboBox cbbSearch = new JComboBox();
		cbbSearch.setModel(new DefaultComboBoxModel(
				new String[] { "Row ID", "First Name", "Last Name", "CUNY ID", "GPA", "Venus Login" }));
		cbbSearch.setBounds(72, 1, 133, 27);
		contentPane.add(cbbSearch);

		txtSearch = new JTextField();
		txtSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				
				// Lấy điều kiện tìm kiếm
				int indexSelected = cbbSearch.getSelectedIndex();
				String keySearch = txtSearch.getText().trim();
				
				// Gọi hàm tìm kiếm
				StudentDAO dao = new StudentDAO();
				try {
					List<Student> listOfStudent = dao.find(indexSelected, keySearch);
					
					// Hiển thị danh sach student lên table 
					// Clear current data
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					while (model.getRowCount() > 0) {
						model.removeRow(model.getRowCount() - 1);
					}
					
					// load new data
					for (Student s : listOfStudent) {
						model.addRow(new Object[] { s.getRowId(), s.getFirstName(), s.getLastName(), s.getCunyId(),
								s.getGpa(), s.getVenusLogin() });
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		txtSearch.setBounds(213, 1, 169, 26);
		contentPane.add(txtSearch);
		txtSearch.setColumns(10);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddStudent addFrm = new AddStudent();
				addFrm.setLocationRelativeTo(contentPane);
				addFrm.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {
						if (addFrm.isAction()){
							updateTableData();
						}
					}
				}); 
				addFrm.setVisible(true);
			}
		});
		btnAdd.setBounds(462, 0, 90, 29);
		contentPane.add(btnAdd);

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Dòng dữ liệu cần xóa
				int rowIdx = table.getSelectedRow();
				
				if (rowIdx != -1) {
					// Tạo đối tượng Student từ dòng được chọn
					Student student = new Student();
					student.setRowId(Long.parseLong(table.getValueAt(rowIdx, 0).toString()));
					student.setFirstName(table.getValueAt(rowIdx, 1).toString());
					student.setLastName(table.getValueAt(rowIdx, 2).toString());
					student.setCunyId(Long.parseLong(table.getValueAt(rowIdx, 3).toString()));
					student.setGpa(Double.parseDouble(table.getValueAt(rowIdx, 4).toString()));
					student.setVenusLogin(table.getValueAt(rowIdx, 5).toString());
					
					DeleteStudent deleteFrm = new DeleteStudent(student);
					deleteFrm.addWindowListener(new WindowAdapter() {
						@Override
						public void windowClosed(WindowEvent e) {
							if (deleteFrm.isAction()) {
								DefaultTableModel model = (DefaultTableModel) table.getModel();
								model.removeRow(rowIdx);
							}
						}
					});
					deleteFrm.setLocationRelativeTo(contentPane);
					deleteFrm.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(contentPane, "No row is selected to delete.");
				}
			}
		});
		btnDelete.setBounds(566, 0, 90, 29);
		contentPane.add(btnDelete);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 34, 650, 529);
		contentPane.add(scrollPane);

		String[] columnNames = { "Row ID", "First Name", "Last Name", "CUNY ID", "GPA", "Venus Login" };
		Object[][] data = new Object[][] {};
		DefaultTableModel model = new DefaultTableModel(data, columnNames);

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// Double-click on row
				if (e.getClickCount() == 2) {
					int rowIdx = table.getSelectedRow();
					Student student = new Student();
					student.setRowId(Long.parseLong(table.getValueAt(rowIdx, 0).toString()));
					student.setFirstName(table.getValueAt(rowIdx, 1).toString());
					student.setLastName(table.getValueAt(rowIdx, 2).toString());
					student.setCunyId(Long.parseLong(table.getValueAt(rowIdx, 3).toString()));
					student.setGpa(Double.parseDouble(table.getValueAt(rowIdx, 4).toString()));
					student.setVenusLogin(table.getValueAt(rowIdx, 5).toString());
					
					ModifyStudent modifyFrm = new ModifyStudent(student);
					modifyFrm.setLocationRelativeTo(contentPane);
					modifyFrm.setVisible(true);
					modifyFrm.addWindowListener(new WindowAdapter() {

						@Override
						public void windowClosed(WindowEvent e) {
							if (modifyFrm.isAction()) {
								table.setValueAt(student.getFirstName(), rowIdx, 1);
								table.setValueAt(student.getLastName(), rowIdx, 2);
								table.setValueAt(student.getCunyId(), rowIdx, 3);
								table.setValueAt(student.getGpa(), rowIdx, 4);
								table.setValueAt(student.getVenusLogin(), rowIdx, 5);
							}
						}
						
					});
				}
			}
		});
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		// Disable edit cell in table row
		table.setDefaultEditor(Object.class, null);
		table.setModel(model);
		scrollPane.setViewportView(table);

		JButton btnExportData = new JButton("Export Data");
		btnExportData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Chọn tên và thu mục cần ghi
				File selected = null;
				JFileChooser jfc = new JFileChooser();
				if (jfc.showOpenDialog(contentPane) == JFileChooser.APPROVE_OPTION) {
					selected = jfc.getSelectedFile();
				}

				// Thực hiện ghi file
				if (selected.getName().endsWith("txt")) {
					writeFileWithByteStream(selected);
				} else if (selected.getName().endsWith("bin")) {
					writeFileWithObject(selected);
				} else if (selected.getName().endsWith("json")) {
					List<Student> students = tableToStudent();
					System.out.println(students.size());
					try {
//						JsonUtils.simpleWriteJson(students, selected.getAbsolutePath());
						JsonUtils.gsonWriteJson(students, selected.getAbsolutePath());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				} else if (selected.getName().endsWith("xml")) {
					List<Student> students = tableToStudent();
					try {
						XMLUtils.writeFile(students, selected.getAbsolutePath());
						JOptionPane.showMessageDialog(null, "Saved successfull.");
					} catch (ParserConfigurationException e1) {
						e1.printStackTrace();
					} catch (TransformerException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		btnExportData.setBounds(265, 572, 117, 29);
		contentPane.add(btnExportData);
		
		// Load danh sách student từ database
		initTableData();
	}

	private void initTableData() {
		StudentDAO dao = new StudentDAO();
		List<Student> listOfStudent;
		try {
			listOfStudent = dao.findAll();
			DefaultTableModel model = (DefaultTableModel) table.getModel();
			
			// Clear current data
			while (model.getRowCount() > 0) {
				model.removeRow(model.getRowCount() - 1);
			}
			
			// load new data
			for (Student s : listOfStudent) {
				model.addRow(new Object[] { s.getRowId(), s.getFirstName(), s.getLastName(), s.getCunyId(),
						s.getGpa(), s.getVenusLogin() });
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Đọc dữ liệu từ JTable -> chuyển thành danh sách Student
	 * @return
	 */
	protected List<Student> tableToStudent() {
		List<Student> list = new ArrayList<Student>();
		
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		for (int idx = 0; idx < model.getRowCount(); idx++) {
			Student student = new Student();
			student.setRowId(Long.parseLong(model.getValueAt(idx, 0).toString()));
			student.setFirstName(model.getValueAt(idx, 1).toString());
			student.setLastName(model.getValueAt(idx, 2).toString());
			student.setCunyId(Long.parseLong(model.getValueAt(idx, 3).toString()));
			student.setGpa(Double.parseDouble(model.getValueAt(idx, 4).toString()));
			student.setVenusLogin(model.getValueAt(idx, 5).toString());

			list.add(student);
		}
		return list;
	}

	protected void writeFileWithObject(File selected) {
		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(selected))) {
			DefaultTableModel model = (DefaultTableModel) table.getModel();
			for (int idx = 0; idx < model.getRowCount(); idx++) {
				Student student = new Student();
				student.setRowId(Long.parseLong(model.getValueAt(idx, 0).toString()));
				student.setFirstName(model.getValueAt(idx, 1).toString());
				student.setLastName(model.getValueAt(idx, 2).toString());
				student.setCunyId(Long.parseLong(model.getValueAt(idx, 3).toString()));
				student.setGpa(Double.parseDouble(model.getValueAt(idx, 4).toString()));
				student.setVenusLogin(model.getValueAt(idx, 5).toString());

				out.writeObject(student);
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected void writeFileWithByteStream(File selected) {
		try (FileOutputStream out = new FileOutputStream(selected)) {
			DefaultTableModel model = (DefaultTableModel) table.getModel();
			for (int idx = 0; idx < model.getRowCount(); idx++) {
				for (int i = 0; i < model.getValueAt(idx, 0).toString().length(); i++) {
					out.write(model.getValueAt(idx, 0).toString().charAt(i));
				}
				out.write(',');
				out.write(' ');

				for (int i = 0; i < model.getValueAt(idx, 1).toString().length(); i++) {
					out.write(model.getValueAt(idx, 1).toString().charAt(i));
				}
				out.write(',');
				out.write(' ');

				for (int i = 0; i < model.getValueAt(idx, 2).toString().length(); i++) {
					out.write(model.getValueAt(idx, 2).toString().charAt(i));
				}
				out.write(',');
				out.write(' ');

				for (int i = 0; i < model.getValueAt(idx, 3).toString().length(); i++) {
					out.write(model.getValueAt(idx, 3).toString().charAt(i));
				}
				out.write(',');
				out.write(' ');

				for (int i = 0; i < model.getValueAt(idx, 4).toString().length(); i++) {
					out.write(model.getValueAt(idx, 4).toString().charAt(i));
				}
				out.write(',');
				out.write(' ');

				for (int i = 0; i < model.getValueAt(idx, 5).toString().length(); i++) {
					out.write(model.getValueAt(idx, 5).toString().charAt(i));
				}
				out.write('\n');
			}
			JOptionPane.showMessageDialog(null, "write file successful.");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void updateTableData() {
		initTableData();
	}
}
