package vn.molu.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import vn.molu.dao.StudentDAO;
import vn.molu.domain.Student;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ModifyStudent extends JFrame {

	private JPanel contentPane;
	private JTextField txtFirstName;
	private JTextField txtLastName;
	private JTextField txtCunyId;
	private JTextField txtGPA;
	private JTextField txtVenusLogin;
	private JTextField txtRowId;
	
	// true -> update successful, ngược lại false
	private boolean action;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					ModifyStudent frame = new ModifyStudent();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public ModifyStudent(Student student) {
		setTitle("Modify Current Student Record");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 314, 275);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblFirstName = new JLabel("First Name:");
		lblFirstName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFirstName.setBounds(6, 47, 95, 16);
		contentPane.add(lblFirstName);
		
		txtFirstName = new JTextField();
		txtFirstName.setBounds(110, 42, 191, 26);
		contentPane.add(txtFirstName);
		txtFirstName.setColumns(10);
		
		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLastName.setBounds(6, 77, 95, 16);
		contentPane.add(lblLastName);
		
		txtLastName = new JTextField();
		txtLastName.setColumns(10);
		txtLastName.setBounds(110, 72, 191, 26);
		contentPane.add(txtLastName);
		
		JLabel lblCunyId = new JLabel("CUNY ID:");
		lblCunyId.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCunyId.setBounds(6, 107, 95, 16);
		contentPane.add(lblCunyId);
		
		txtCunyId = new JTextField();
		txtCunyId.setColumns(10);
		txtCunyId.setBounds(110, 102, 191, 26);
		contentPane.add(txtCunyId);
		
		JLabel lblGpa = new JLabel("GPA:");
		lblGpa.setHorizontalAlignment(SwingConstants.RIGHT);
		lblGpa.setBounds(6, 140, 95, 16);
		contentPane.add(lblGpa);
		
		txtGPA = new JTextField();
		txtGPA.setColumns(10);
		txtGPA.setBounds(110, 135, 191, 26);
		contentPane.add(txtGPA);
		
		JLabel lblVenusLogin = new JLabel("Venus Login:");
		lblVenusLogin.setHorizontalAlignment(SwingConstants.RIGHT);
		lblVenusLogin.setBounds(6, 174, 95, 16);
		contentPane.add(lblVenusLogin);
		
		txtVenusLogin = new JTextField();
		txtVenusLogin.setColumns(10);
		txtVenusLogin.setBounds(110, 169, 191, 26);
		contentPane.add(txtVenusLogin);
		
		JButton btnAddNewStudent = new JButton("Update Student Record");
		btnAddNewStudent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// update student
				student.setFirstName(txtFirstName.getText().trim());
				student.setLastName(txtLastName.getText().trim());
				student.setCunyId(Long.parseLong(txtCunyId.getText().trim()));
				student.setGpa(Double.parseDouble(txtGPA.getText().trim()));
				student.setVenusLogin(txtVenusLogin.getText().trim());
				
				// update database
				StudentDAO dao = new StudentDAO();
				try {
					if (dao.update(student) > 0) {
						action = true;
						dispose();
					}
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(contentPane, "Modify student fail.");
					ex.printStackTrace();
				}
				
			}
		});
		btnAddNewStudent.setForeground(Color.BLACK);
		btnAddNewStudent.setBackground(Color.BLUE);
		btnAddNewStudent.setBounds(16, 218, 173, 29);
		contentPane.add(btnAddNewStudent);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancel.setBounds(190, 218, 95, 29);
		contentPane.add(btnCancel);
		
		JLabel lblRowId = new JLabel("Row ID:");
		lblRowId.setHorizontalAlignment(SwingConstants.RIGHT);
		lblRowId.setBounds(6, 11, 95, 16);
		contentPane.add(lblRowId);
		
		txtRowId = new JTextField();
		txtRowId.setEditable(false);
		txtRowId.setColumns(10);
		txtRowId.setBounds(110, 6, 191, 26);
		contentPane.add(txtRowId);
		
		// load gia tri cho cac truong
		txtRowId.setText(String.valueOf(student.getRowId()));
		txtFirstName.setText(student.getFirstName());
		txtLastName.setText(student.getLastName());
		txtCunyId.setText(String.valueOf(student.getCunyId()));
		txtGPA.setText(String.valueOf(student.getGpa()));
		txtVenusLogin.setText(student.getVenusLogin());
	}
	
	public boolean isAction() {
		return action;
	}
}
