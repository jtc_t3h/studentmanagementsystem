package vn.molu.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import vn.molu.domain.Student;

public class AbstractDAO {

	/**
	 * Lớp thể hiện trình điều khiển đã nạp vào hệ thống Nếu lớp này tồn tại thì đã
	 * nạp trình điều khiển, ngược lại chưa nạp
	 * 
	 * Tùy theo hệ quản trị mà chuỗi Driver khác nhau
	 */
	private static final String DRIVER_MANAGER_CLASS = "com.mysql.cj.jdbc.Driver";
	static {
		try {
			Class.forName(DRIVER_MANAGER_CLASS);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static final String USERNAME = "root";
	private static final String PASSWORD = "";

	protected static Connection con;
	protected static Statement st;
	protected static PreparedStatement pst;
	protected static ResultSet rs;

	public static void openConnection() {
		try {
			con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			System.out.println("Connect successful.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void closeConnection() {
		try {
			if (rs != null) rs.close();
			if (st != null) st.close();
			if (pst != null) pst.close();
			if (con != null) con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Định dạng chuỗi kết nối phụ thuộc vào hệ quản trị CSDL
	 */
	private static final String URL = "jdbc:mysql://localhost:3306/studentdb";

	public static void main(String[] args) {

		openConnection();

		try {
			// B4. Tao doi tuong statement
			Statement st = con.createStatement();

			// B5. Tao cau SQL, va thuc thi cau SQL
			String sqlInsert = "INSERT INTO student VALUES (3, 'Pham', 'De', 1, 1.1, '333333')";
			st.executeUpdate(sqlInsert);

			String sqlSelect = "select * from student";
			ResultSet rs = st.executeQuery(sqlSelect);

			// B6. Xử lý resultSet vào đưa vào đối tượng Student
			while (rs.next()) {
				int rowId = rs.getInt(1);
				String firstName = rs.getString("first_name");

				Student stu = new Student();
				stu.setRowId(rowId);
				stu.setFirstName(firstName);
				stu.setLastName(rs.getString("last_name"));
				stu.setCunyId(rs.getInt("cuny_id"));
				stu.setGpa(rs.getDouble("gpa"));
				stu.setVenusLogin(rs.getString(6));

				System.out.println(stu.getRowId());
			}

			// B7. Đóng kết nối
			rs.close();
			st.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
