package vn.molu.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import vn.molu.domain.Student;

public class StudentDAO extends AbstractDAO {

	public List<Student> findAll() throws SQLException {
		List<Student> listOfStudent = new ArrayList<Student>();
		openConnection();
		
		st = con.createStatement();
		String select = "select * from student";
		rs = st.executeQuery(select);
		while(rs.next()) {
			Student student = new Student();
			student.setRowId(rs.getLong(1));
			student.setFirstName(rs.getString(2));
			student.setLastName(rs.getString(3));
			student.setCunyId(rs.getLong(4));
			student.setGpa(rs.getDouble(5));
			student.setVenusLogin(rs.getString(6));
			
			listOfStudent.add(student);
		}
		
		closeConnection();
		return listOfStudent;
	}

	public int insert(Student student) throws SQLException {
		int result = 0;
		openConnection();
		
		String sqlInsert = "insert into student (first_name, last_name, cuny_id, gpa, venus_login) values (?,?,?,?,?)";
		pst = con.prepareStatement(sqlInsert);
		pst.setString(1, student.getFirstName());
		pst.setString(2, student.getLastName());
		pst.setLong(3, student.getCunyId());
		pst.setDouble(4, student.getGpa());
		pst.setString(5, student.getVenusLogin());
		result = pst.executeUpdate();
		
		closeConnection();
		
		return result;
	}

	public int update(Student student) throws SQLException {
		int result = 0;
		openConnection();
		
		String sqlInsert = "update student set first_name =?, last_name = ?, cuny_id = ?, gpa = ?, venus_login = ? where row_id = ?";
		pst = con.prepareStatement(sqlInsert);
		pst.setString(1, student.getFirstName());
		pst.setString(2, student.getLastName());
		pst.setLong(3, student.getCunyId());
		pst.setDouble(4, student.getGpa());
		pst.setString(5, student.getVenusLogin());
		pst.setLong(6, student.getRowId());
		result = pst.executeUpdate();
		
		closeConnection();

		return result;

	}
	
	public int delete(Long rowId) throws SQLException {
		int result = 0;
		openConnection();
		
		String sqlDelete = "delete from student where row_id = ?";
		pst = con.prepareStatement(sqlDelete);
		pst.setLong(1, rowId);
		result = pst.executeUpdate();
		
		closeConnection();
		return result;

	}

	public List<Student> find(int indexSelected, String keySearch) throws NumberFormatException, SQLException {
		List<Student> listOfStudent = null;
		
		switch (indexSelected) {
		case 0:
			listOfStudent = findByRowId(Long.parseLong(keySearch));
			break;
		case 1:
			listOfStudent = findByFirstName(keySearch);
			break;
		default:
			break;
		}

		return listOfStudent;
	}

	private List<Student> findByFirstName(String keySearch) {
		// TODO Auto-generated method stub
		return null;
	}

	private List<Student> findByRowId(long rowId) throws SQLException {
		List<Student> listOfStudent = new ArrayList<Student>();
		openConnection();
		
		st = con.createStatement();
		String select = "select * from student where row_id = " + rowId;
		rs = st.executeQuery(select);
		while(rs.next()) {
			Student student = new Student();
			student.setRowId(rs.getLong(1));
			student.setFirstName(rs.getString(2));
			student.setLastName(rs.getString(3));
			student.setCunyId(rs.getLong(4));
			student.setGpa(rs.getDouble(5));
			student.setVenusLogin(rs.getString(6));
			
			listOfStudent.add(student);
		}
		
		closeConnection();
		return listOfStudent;
	}
}
