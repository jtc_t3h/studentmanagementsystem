package vn.molu.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;

import vn.molu.domain.Student;

public class JsonUtils {

	/**
	 * Đọc dữ liệu từ file theo định dạng JSON. Sau đó chuyển dữ liệu thành danh sách Student
	 * @param path
	 * 			Đường dẫn của file JSON
	 * @return
	 * 			Danh sách Student
	 * @throws ParseException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public static List<Student> simpleReadJson(String path) throws FileNotFoundException, IOException, ParseException{
		List<Student> list = new ArrayList<Student>();
		
		// Đọc dữ liệu từ JSON dùng thư viên SIMPLE JSON
		// B1
		JSONParser parser = new JSONParser();
		// B2: Đọc dữ liệu từ file -> trả về Object
		Object obj = parser.parse(new FileReader(path));
		// B3: chuyển Object về JSONArray hoặc JSONObject -> trong trường hợp của mình là JSONArray
		JSONArray students = (JSONArray) obj;
		
		// Duyệt array để lấy từng đối tượng
		for (int idx = 0; idx < students.size(); idx++) {
			JSONObject temp = (JSONObject) students.get(idx);
			
			Student student = new Student();
			student.setRowId(Long.parseLong(temp.get("rowId").toString()));
			student.setFirstName(temp.get("firstName").toString());
			student.setLastName(temp.get("lastName").toString());
			student.setCunyId(Long.parseLong(temp.get("cunyId").toString()));
			student.setGpa(Double.parseDouble(temp.get("gpa").toString()));
			student.setVenusLogin(temp.get("venusLogin").toString());
			
			list.add(student);
		}
		
		return list;
	}
	
	public static void simpleWriteJson(List<Student> listOfStudent, String path) throws IOException {
		JSONArray students = new JSONArray();
		
		for (Student s: listOfStudent) {
			// Tạo ra đối tượng JSONObject tương ứng cho từng đối tượng Student
			JSONObject student = new JSONObject();
			student.put("rowId", s.getRowId());
			student.put("firstName", s.getFirstName());
			
			
			// Add JSONObject vào JSONArray
			students.add(student);
		}
		
		// Ghi vào file
		try (FileWriter out = new FileWriter(path)){
			out.write(students.toJSONString());
		}
	}
	
	public static List<Student> gSonReadJson(String path) throws IOException{
		Gson gson = new Gson();
		
		try (Reader reader = new FileReader(path)){
			Student[] students = gson.fromJson(reader, Student[].class);
			
			// convert array -> list
			List<Student> list = new ArrayList<Student>();
			for (Student student: students) {
				list.add(student);
			}
			
			return list;
		}
	}
	
	public static void gsonWriteJson(List<Student> listOfStudent, String path) throws JsonIOException, IOException {
		Gson gson = new Gson();
		try (FileWriter out = new FileWriter(path)){
			gson.toJson(listOfStudent, out);
			out.flush();
		}
		
	}
	
	public static void main(String[] args) throws IOException {
		String path = "/Users/phaolo/student.json";
		
		List<Student> list = gSonReadJson(path);
		System.out.println(list.size());
		for (Student s: list) {
			System.out.println(s);
		}
	}
}
