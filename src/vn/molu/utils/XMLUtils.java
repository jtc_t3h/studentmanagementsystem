package vn.molu.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import vn.molu.domain.Student;

public  class XMLUtils {

	public static List<Student> readXML(String path) throws ParserConfigurationException, SAXException, IOException{
		List<Student> list = new ArrayList<Student>();
		
		// Đối tượng DocumentBuilderFactory
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		// Đối tượng DocumentBuilder
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		// Tạo đối tượng Document -> Read | Write?
		Document doc = builder.parse(new File(path));
		
		// Root Element = Root Node
		Element studentsTag = doc.getDocumentElement();
		System.out.println(studentsTag.getNodeName());
		
		// Duyệt các node con của Students
		NodeList studentsChildNodes = studentsTag.getChildNodes();
		System.out.println(studentsChildNodes.getLength());
		
		for (int idx = 0; idx < studentsChildNodes.getLength(); idx++) {
			Node studentNode = studentsChildNodes.item(idx);
			// Chỉ xử lý node Student -> chỉ có 2 node student
			if (studentNode.getNodeName().equals("student")) {
				NodeList studentChildNodes = studentNode.getChildNodes();
				
				Student student = new Student();
				System.out.println(studentChildNodes.item(1).getTextContent());
				student.setRowId(Long.parseLong(studentChildNodes.item(1).getTextContent()));
				System.out.println(studentChildNodes.item(3).getTextContent());
				student.setFirstName(studentChildNodes.item(3).getTextContent());
				/// Thiết lập cho các thuộc tính khác
				
				list.add(student);
			}
		}
		
		
		
		return list;
	}
	
	public static void main(String[] args) {
		try {
			readXML("/Users/phaolo/students.xml");
		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/** 
	 * Đọc dữ liệu từ danh sách student và ghi vào file
	 * @param students
	 * 			Danh sách dữ liệu cần ghi
	 * @param absolutePath
	 * 			Đường dẫn của file
	 * @throws ParserConfigurationException 
	 * @throws TransformerException 
	 */
	public static void writeFile(List<Student> listOfStudent, String absolutePath) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		// document chính là mô hình
		Document document = builder.newDocument();
		
		// students Node
		Element students = document.createElement("students");
		document.appendChild(students);
		
		for (Student s: listOfStudent) {
			Element student = document.createElement("student");
			students.appendChild(student);
			
			Element rowId = document.createElement("rowId");
			rowId.setTextContent(String.valueOf(s.getRowId()));
			student.appendChild(rowId);
			
			Element firstName = document.createElement("firstName");
			firstName.setTextContent(s.getFirstName());
			student.appendChild(firstName);
			
			// Tương tự cho các node: lastName, cunyId, vernus login
			
		}
		
		// Thực hiện chuyển đổi mô hình dom từ document -> ghi vào file 
		// Tạo đối tượng TransformerFactory và đối tượng Transformer
		TransformerFactory trFactory = TransformerFactory.newInstance();
		Transformer transformer = trFactory.newTransformer();
		
		// Tạo đối tượng DomSource từ mô hình dom document
		DOMSource source = new DOMSource(document);
		
		// Tạo ra đối tượng StreamResult -> chính là file cần ghi
		StreamResult destination = new StreamResult(new File(absolutePath));
		
		// Thực hiện chuyển đổi dom -> file
		transformer.transform(source, destination);
	}
}
